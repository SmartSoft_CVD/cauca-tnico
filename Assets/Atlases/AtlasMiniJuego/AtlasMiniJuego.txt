{"frames": {

"barra_4x.png":
{
	"frame": {"x":2707,"y":313,"w":113,"h":212},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":113,"h":212},
	"sourceSize": {"w":113,"h":212}
},
"boton.png":
{
	"frame": {"x":1393,"y":2483,"w":450,"h":175},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":450,"h":175},
	"sourceSize": {"w":450,"h":175}
},
"boton_4x.png":
{
	"frame": {"x":2570,"y":2247,"w":176,"h":176},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":176,"h":176},
	"sourceSize": {"w":176,"h":176}
},
"btn_ayuda_4x.png":
{
	"frame": {"x":2615,"y":965,"w":218,"h":217},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":218,"h":217},
	"sourceSize": {"w":218,"h":217}
},
"btn_dificil_4x.png":
{
	"frame": {"x":2043,"y":1041,"w":570,"h":210},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":570,"h":210},
	"sourceSize": {"w":570,"h":210}
},
"btn_facil_4x.png":
{
	"frame": {"x":2043,"y":1253,"w":570,"h":207},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":570,"h":207},
	"sourceSize": {"w":570,"h":207}
},
"btn_iniciar_4x.png":
{
	"frame": {"x":2,"y":2483,"w":904,"h":362},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":904,"h":362},
	"sourceSize": {"w":904,"h":362}
},
"btn_iniciar_Facebook4x.png":
{
	"frame": {"x":908,"y":2483,"w":483,"h":148},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":483,"h":148},
	"sourceSize": {"w":483,"h":148}
},
"btn_medio_4x.png":
{
	"frame": {"x":2043,"y":1253,"w":570,"h":207},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":570,"h":207},
	"sourceSize": {"w":570,"h":207}
},
"circulo.png":
{
	"frame": {"x":2589,"y":254,"w":113,"h":92},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":113,"h":92},
	"sourceSize": {"w":113,"h":92}
},
"cuerdas_dificil_4x.png":
{
	"frame": {"x":2393,"y":572,"w":292,"h":108},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":292,"h":108},
	"sourceSize": {"w":292,"h":108}
},
"cuerdas_facil_4x.png":
{
	"frame": {"x":1845,"y":2483,"w":290,"h":242},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":290,"h":242},
	"sourceSize": {"w":290,"h":242}
},
"cuerdas_iniciar_4x.png":
{
	"frame": {"x":2369,"y":718,"w":451,"h":245},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":451,"h":245},
	"sourceSize": {"w":451,"h":245}
},
"cuerdas_medio_4x.png":
{
	"frame": {"x":2393,"y":572,"w":292,"h":108},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":292,"h":108},
	"sourceSize": {"w":292,"h":108}
},
"estrella_grande.png":
{
	"frame": {"x":2657,"y":2587,"w":166,"h":158},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":166,"h":158},
	"sourceSize": {"w":166,"h":158}
},
"estrella_pequena.png":
{
	"frame": {"x":2687,"y":572,"w":140,"h":138},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":140,"h":138},
	"sourceSize": {"w":140,"h":138}
},
"fondo.png":
{
	"frame": {"x":2570,"y":2002,"w":240,"h":243},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":240,"h":243},
	"sourceSize": {"w":240,"h":243}
},
"fondo_4x.png":
{
	"frame": {"x":2043,"y":718,"w":324,"h":321},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":324,"h":321},
	"sourceSize": {"w":324,"h":321}
},
"fondo_Juego_4x.png":
{
	"frame": {"x":2043,"y":362,"w":348,"h":354},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":348,"h":354},
	"sourceSize": {"w":348,"h":354}
},
"fondo_marcos.png":
{
	"frame": {"x":2615,"y":1184,"w":188,"h":183},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":188,"h":183},
	"sourceSize": {"w":188,"h":183}
},
"fondo_nivel.png":
{
	"frame": {"x":908,"y":2633,"w":168,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":168,"h":128},
	"sourceSize": {"w":168,"h":128}
},
"fondo_texto.png":
{
	"frame": {"x":2704,"y":254,"w":88,"h":57},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":57},
	"sourceSize": {"w":88,"h":57}
},
"g.png":
{
	"frame": {"x":2748,"y":2247,"w":85,"h":118},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":0,"w":85,"h":118},
	"sourceSize": {"w":113,"h":118}
},
"logo_4x.png":
{
	"frame": {"x":2043,"y":2,"w":544,"h":358},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":544,"h":358},
	"sourceSize": {"w":544,"h":358}
},
"marco_con_fondo.png":
{
	"frame": {"x":2570,"y":1732,"w":273,"h":268},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":273,"h":268},
	"sourceSize": {"w":273,"h":268}
},
"marco_sin_fondo.png":
{
	"frame": {"x":2570,"y":1462,"w":273,"h":268},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":273,"h":268},
	"sourceSize": {"w":273,"h":268}
},
"progreso_barra_4x.png":
{
	"frame": {"x":2570,"y":2425,"w":85,"h":161},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":85,"h":161},
	"sourceSize": {"w":85,"h":161}
},
"reloj.png":
{
	"frame": {"x":2589,"y":2,"w":250,"h":250},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":250,"h":250},
	"sourceSize": {"w":250,"h":250}
},
"reloj_4x.png":
{
	"frame": {"x":2657,"y":2425,"w":161,"h":160},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":161,"h":160},
	"sourceSize": {"w":161,"h":160}
},
"score_4x.png":
{
	"frame": {"x":2393,"y":362,"w":312,"h":208},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":312,"h":208},
	"sourceSize": {"w":312,"h":208}
},
"transparencia_4x.png":
{
	"frame": {"x":2,"y":2,"w":2039,"h":1530},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":2039,"h":1530},
	"sourceSize": {"w":2039,"h":1530}
},
"tuto1_4x.png":
{
	"frame": {"x":1286,"y":1534,"w":1282,"h":947},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":1282,"h":947},
	"sourceSize": {"w":1282,"h":947}
},
"tuto2_4x.png":
{
	"frame": {"x":2,"y":1534,"w":1282,"h":947},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":1282,"h":947},
	"sourceSize": {"w":1282,"h":947}
},
"v.png":
{
	"frame": {"x":2615,"y":1369,"w":85,"h":77},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":85,"h":77},
	"sourceSize": {"w":85,"h":77}
},
"y.png":
{
	"frame": {"x":2702,"y":1369,"w":57,"h":83},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":0,"w":57,"h":83},
	"sourceSize": {"w":85,"h":83}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "AtlasMiniJuego.png",
	"format": "RGBA8888",
	"size": {"w":2847,"h":2847},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:6515ffe962457dec70e992f106551c0e:fdd6106f1003d7cbf28e6795c7644946:48e17636cceb593da94f7019983a639a$"
}
}
