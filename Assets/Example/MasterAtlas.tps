<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <false/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity</string>
        <key>textureFileName</key>
        <filename>../Resources/Atlases/MasterAtlas.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">NPOT</enum>
            <key>forceSquared</key>
            <true/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>../Resources/Atlases/MasterAtlas.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>../Resources/Atlases/MasterAtlas.txt</filename>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Yellow/Alpha/idle.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Yellow/Beta/idle1.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Yellow/Delta/idle.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Yellow/Delta/idle3.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Yellow/Gamma/idle4.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Yellow/Zeta/idle5.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Red/Alpha/idle6.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Red/Beta/idle7.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Red/Delta/idle8.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Red/Gamma/idle9.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Red/Zeta/idle10.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Purple/Alpha/idle11.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Purple/Beta/idle12.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Purple/Delta/idle13.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Purple/Gamma/idle14.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Purple/Zeta/idle15.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Green/Alpha/idle16.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Green/Beta/idle17.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Green/Delta/idle18.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Green/Gamma/idle19.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Green/Zeta/idle20.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Beige/Alpha/idle21.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Beige/Beta/idle22.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Beige/Delta/idle23.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Beige/Gamma/idle24.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Beige/Zeta/idle25.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Blue/Alpha/idle26.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Blue/Beta/idle27.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Blue/Delta/idle28.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Blue/Gamma/idle29.png</filename>
            <filename>../../../../../Downloads/platformerGraphics_xenoDiversity/Blue/Zeta/idle30.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
