﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

	private bool mute;
	private UISprite sprite;

	// Use this for initialization
	void Start ()
	{
		sprite = this.gameObject.GetComponentInChildren<UISprite>();

		if(PlayerPrefsX.GetBool("Mute"))
		{
			sprite.spriteName = "btn_sound_desact_4x";
		}
	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void MuteGame()
	{
		mute = !mute;

		if(mute)
		{
			PlayerPrefsX.SetBool("Mute", true);
			sprite.spriteName = "btn_sound_desact_4x";
		}
		else
		{
			PlayerPrefsX.SetBool("Mute", false);
			sprite.spriteName = "btn_sound_act_4x";
		}
	}


}
