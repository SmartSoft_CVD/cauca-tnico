﻿using UnityEngine;
using System.Collections;

public class LoginFacebook : MonoBehaviour
{

	public UIButton [] facebookButtons;

	// Use this for initialization
	void Start ()
	{
		EventDelegate.Add (facebookButtons[0].onClick, delegate () {
				if (!FB.IsLoggedIn) {
						CallFBLogin ();
				}

		});

		EventDelegate.Add (facebookButtons[1].onClick, delegate () {
			if (FB.IsLoggedIn) {
				FB.Logout();
			}
			
		});

	}

	private void CallFBLogin ()
	{
		FB.Login ("email,publish_actions", LoginCallback);
	}

	void LoginCallback (FBResult result)
	{
		if (result != null) {
				Debug.LogError ("Error: " + result.Error);
		} else {
				Debug.Log ("Inicio Sesion");
		}
	}

	void Update()
	{
		if(!FB.IsLoggedIn)
		{
			NGUITools.SetActive(facebookButtons[0].gameObject, true);
			NGUITools.SetActive(facebookButtons[1].gameObject, false);
		}
		else
		{
			NGUITools.SetActive(facebookButtons[0].gameObject, false);
			NGUITools.SetActive(facebookButtons[1].gameObject, true);
		}
	}
}
