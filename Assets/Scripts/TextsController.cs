﻿using UnityEngine;
using System.Collections;

public class TextsController : MonoBehaviour {

	public ArregloMultidimensional [] textosCampesinos = new ArregloMultidimensional[4];
	public ArregloMultidimensional [] textosAfro = new ArregloMultidimensional[4];
	public ArregloMultidimensional [] textosGuambianos = new ArregloMultidimensional[4];
	public ArregloMultidimensional [] textosNasa = new ArregloMultidimensional[4];
	public ArregloMultidimensional [] textosYanaconas = new ArregloMultidimensional[4];

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
	}
}
