﻿using UnityEngine;
using System.Collections;

public class ControlTutorialPausedGame : MonoBehaviour {

	public UIButton openTuto;
	public UIButton closeTuto;
	private BoxCollider boxCollider;
	public bool pausedGame;
	public bool estaEnJuego = false;

	public GameObject botonReiniciar;

	private GameObject hudMI_Parent;
	public string rootHUD;
	// Referencia al objeto "HUD" en la jerarquia
	private GameObject hudMainParent;

	void Awake()
	{
		boxCollider = this.gameObject.GetComponent<BoxCollider>();
	}

	// Use this for initialization
	void Start ()
	{
		rootHUD = "UI Root/Camera/HUD/";
		// Referencia al objeto "HUD" en la jerarquia
		hudMainParent = GameObject.Find (rootHUD);

		boxCollider.enabled = false;

		EventDelegate.Add(openTuto.onClick, delegate()
  		{
			boxCollider.enabled = true;
			pausedGame = true;
			Time.timeScale = 0;
		});

		EventDelegate.Add(closeTuto.onClick, delegate()
  		{
			boxCollider.enabled = false;
			pausedGame = false;
			Time.timeScale = 1;
		});

		if(estaEnJuego)
			openTuto.gameObject.SendMessage("OnClick");
	}

	public void Menu()
	{
		GoToMenu();
	}

	void GoToMenu()
	{
		PlayerPrefsX.SetBool("Inicio", false);


		hudMI_Parent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Mini_Juego_Inicio"));
		
		
		// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
		hudMI_Parent.name = "Mini juego Inicio";
		
		// Lo agregamos como hijo del HUD
		hudMI_Parent.transform.parent = hudMainParent.transform;
		
		// La asignaciom modifica el Transform, asi que lo reiniciamos
		hudMI_Parent.transform.localPosition = Vector3.zero;
		hudMI_Parent.transform.localRotation = Quaternion.identity;
		hudMI_Parent.transform.localScale = Vector3.one;

//		Time.timeScale = 1.0f;
	}
	
	public void MenuPrincipal()
	{
		StartCoroutine(GoToMenuPrincipal());
	}
	
	IEnumerator GoToMenuPrincipal()
	{

		Application.LoadLevel(0);
		yield return new WaitForEndOfFrame();
		PlayerPrefsX.SetBool("Inicio", true);
		
		
		
	}
}
