﻿using UnityEngine;
using System.Collections;

public class DestroyLegacyParticles : MonoBehaviour {

	ParticleEmitter emisor;

	public GameObject parte1;
	public GameObject parte2;
	public GameObject parte3;
	public GameObject parte4;
	public GameObject parte5;

//	private ParticleEmitter emisor1;
//	private ParticleEmitter emisor2;
//	private ParticleEmitter emisor3;
//	private ParticleEmitter emisor4;
//	private ParticleEmitter emisor5;

	void Start()
	{

		StartCoroutine(InstanciarPrefabs());
	}


	IEnumerator InstanciarPrefabs()
	{
		yield return new WaitForSeconds(1.0f);

		if (parte1 != null) {
			
			// Desactivamos la pantalla actual
			parte1.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (parte1);
			parte1 = null;
		}
		
		if (parte2 != null) {
			
			// Desactivamos la pantalla actual
			parte2.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (parte2);
			parte2 = null;
		}
		
		
		if (parte3 != null) {
			
			// Desactivamos la pantalla actual
			parte3.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (parte3);
			parte3 = null;
		}
		
		if (parte4 != null) {
			
			// Desactivamos la pantalla actual
			parte4.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (parte4);
			parte4 = null;
		}
		
		if (parte5 != null) {
			
			// Desactivamos la pantalla actual
			parte5.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (parte5);
			parte5 = null;
		}

		#region
		if(parte1 == null)
		{
			parte1 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Violet"));		
			parte1.name = "Violet";
			parte1.transform.parent = this.transform;		
			parte1.transform.localPosition = Vector3.zero;
			parte1.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte1.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte2 == null)
		{
			parte2 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Yellow"));		
			parte2.name = "Yellow";
			parte2.transform.parent = this.transform;		
			parte2.transform.localPosition = Vector3.zero;
			parte2.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte2.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte3 == null)
		{
			parte3 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Green"));		
			parte3.name = "Green";
			parte3.transform.parent = this.transform;		
			parte3.transform.localPosition = Vector3.zero;
			parte3.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte3.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte4 == null)
		{
			parte4 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Blue"));		
			parte4.name = "Blue";
			parte4.transform.parent = this.transform;		
			parte4.transform.localPosition = Vector3.zero;
			parte4.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte4.transform.localScale = Vector3.one;
		}
		#endregion
		
		#region
		if(parte5 == null)
		{
			parte5 = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Particles/Red"));		
			parte5.name = "Red";
			parte5.transform.parent = this.transform;		
			parte5.transform.localPosition = Vector3.zero;
			parte5.transform.localRotation = new Quaternion(90.0f, 180f, 0, 0);
			parte5.transform.localScale = Vector3.one;
		}
		#endregion
	}
	
	//	// Use this for initialization
//	void Start ()
//	{
//		if (parte1 != null) {
//			
//			// Desactivamos la pantalla actual
//			parte1.SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (parte1);
//			parte1 = null;
//		}
//
//		if (parte2 != null) {
//			
//			// Desactivamos la pantalla actual
//			parte2.SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (parte2);
//			parte2 = null;
//		}
//
//
//		if (parte3 != null) {
//			
//			// Desactivamos la pantalla actual
//			parte3.SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (parte3);
//			parte3 = null;
//		}
//
//		if (parte4 != null) {
//			
//			// Desactivamos la pantalla actual
//			parte4.SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (parte4);
//			parte4 = null;
//		}
//
//		if (parte5 != null) {
//			
//			// Desactivamos la pantalla actual
//			parte5.SetActive (false); // (opcional)
//			
//			// Eliminamos HUD padre
//			GameObject.Destroy (parte5);
//			parte5 = null;
//		}
//
//		parte1 = this.transform.FindChild("Violet").gameObject;
//		parte2 = this.transform.FindChild("Yellow").gameObject;
//		parte3 = this.transform.FindChild("Green").gameObject;
//		parte4 = this.transform.FindChild("Blue").gameObject;
//		parte5 = this.transform.FindChild("Red").gameObject;
//
//		parte1.SetActive(true);
//		parte2.SetActive(true);
//		parte3.SetActive(true);
//		parte4.SetActive(true);
//		parte5.SetActive(true);
//
//
//		if (emisor1 != null)
//		{
//			// Eliminamos HUD padre
//			GameObject.Destroy (emisor1);
//			emisor1 = null;
//		}
//
//		if (emisor2 != null)
//		{
//			// Eliminamos HUD padre
//			GameObject.Destroy (emisor2);
//			emisor2 = null;
//		}
//
//		if (emisor3 != null)
//		{
//			// Eliminamos HUD padre
//			GameObject.Destroy (emisor3);
//			emisor3 = null;
//		}
//
//		if (emisor4 != null)
//		{
//			// Eliminamos HUD padre
//			GameObject.Destroy (emisor4);
//			emisor4 = null;
//		}
//
//		if (emisor5 != null)
//		{
//			// Eliminamos HUD padre
//			GameObject.Destroy (emisor5);
//			emisor5 = null;
//		}
//
//		emisor1 = parte1.GetComponent<ParticleEmitter>();
//		emisor2 = parte2.GetComponent<ParticleEmitter>();
//		emisor3 = parte3.GetComponent<ParticleEmitter>();
//		emisor4 = parte4.GetComponent<ParticleEmitter>();
//		emisor5 = parte5.GetComponent<ParticleEmitter>();
//
////		emisor1.ClearParticles();
////		emisor2.ClearParticles();
////		emisor3.ClearParticles();
////		emisor4.ClearParticles();
////		emisor5.ClearParticles();
//
//		emisor1.enabled = true;
//		emisor2.enabled = true;
//		emisor3.enabled = true;
//		emisor4.enabled = true;
//		emisor5.enabled = true;
//
//		emisor1.emit = true;
//		emisor2.emit = true;
//		emisor3.emit = true;
//		emisor4.emit = true;
//		emisor5.emit = true;
//
////		emisor1.Emit();
////		emisor2.Emit();
////		emisor3.Emit();
////		emisor4.Emit();
////		emisor5.Emit();
//	
//	}
	
	// Update is called once per frame
	void Update ()
	{
//		if(emisor1 != null)
//		{
////			emisor1.ClearParticles();
//			emisor1.Emit();
//		}
//
//		if(emisor2 != null)
//		{
////			emisor2.ClearParticles();
//			emisor2.Emit();
//		}
//
//		if(emisor3 != null)
//		{
////			emisor3.ClearParticles();
//			emisor3.Emit();
//		}
//
//		if(emisor4 != null)
//		{
////			emisor4.ClearParticles();
//			emisor4.Emit();
//		}
//
//		if(emisor5 != null)
//		{
////			emisor5.ClearParticles();
//			emisor5.Emit();
//		}

		//Destroy(this.gameObject, 3.0f);

	
	}
}
