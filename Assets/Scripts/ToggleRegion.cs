﻿using UnityEngine;
using System.Collections;

public class ToggleRegion : MonoBehaviour
{
	public UIButton botonContinuar;

	// Use this for initialization
	void Start ()
	{
		botonContinuar.isEnabled  = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SelectRegion()
	{
		if(UIToggle.current.name == "ButtonAfro" && UIToggle.current.value && UIToggle.current.group == 5)
		{
			PlayerPrefs.SetString("RegionActual", "Afro");
			botonContinuar.isEnabled  = true;
		}
		
		if(UIToggle.current.name == "ButtonCampesinos" && UIToggle.current.value  && UIToggle.current.group == 5)
		{
			PlayerPrefs.SetString("RegionActual", "Campesinos");
			botonContinuar.isEnabled  = true;
		}
		
		if(UIToggle.current.name == "ButtonGuambiano" && UIToggle.current.value  && UIToggle.current.group == 5)
		{
			PlayerPrefs.SetString("RegionActual", "Guambianos");
			botonContinuar.isEnabled  = true;
		}

		if(UIToggle.current.name == "ButtonNasa" && UIToggle.current.value  && UIToggle.current.group == 5)
		{
			PlayerPrefs.SetString("RegionActual", "Nasa");
			botonContinuar.isEnabled  = true;
		}

		if(UIToggle.current.name == "ButtonYanaconas" && UIToggle.current.value  && UIToggle.current.group == 5)
		{
			PlayerPrefs.SetString("RegionActual", "Yanaconas");
			botonContinuar.isEnabled  = true;
		}
	}
}
