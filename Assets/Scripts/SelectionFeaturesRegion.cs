﻿using UnityEngine;
using System.Collections;

public class SelectionFeaturesRegion : MonoBehaviour {

	private string regionCurrent;
	public GameObject gridItemsCurrent;
	public GameObject parentGrids;

	public UIScrollBar scrollBar;
	private UIScrollView scrollView;

	void Awake()
	{
		regionCurrent = PlayerPrefs.GetString("RegionActual");
		scrollView = parentGrids.GetComponent<UIScrollView>();
	}

	// Use this for initialization
	void Start ()
	{

		if (gridItemsCurrent != null) {
			
			// Desactivamos la pantalla actual
			gridItemsCurrent.SetActive (false); // (opcional)
			
			// Eliminamos HUD padre
			GameObject.Destroy (gridItemsCurrent);
			gridItemsCurrent = null;
		}

		switch (regionCurrent)
		{
			case "Campesinos":


			scrollView.ResetPosition();
//			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Caracteristicas/GridCaracteristicasCampesinos"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Campesinos";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-308.5389f, -150, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();

//			UIDragScrollView d;
//			d.scrollView.UpdateScrollbars



			scrollView.ResetPosition();
			scrollBar.ForceUpdate();



			//CambiarColor(0);
			
			
			break;
			case "Afro":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Caracteristicas/GridCaracteristicasAfros"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Afro";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-308.5389f, -150, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			
			//CambiarColor(1);
			
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			break;
			case "Guambianos":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Caracteristicas/GridCaracteristicasGuambianos"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Guambianos";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-308.5389f, -150, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			
			//CambiarColor(2);
			break;
			case "Nasa":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Caracteristicas/GridCaracteristicasNasa"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Nasa";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-308.5389f, -150, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			scrollBar.ForceUpdate();
			
			//CambiarColor(3);
			break;
			case "Yanaconas":
			
			scrollView.ResetPosition();
			//Caras
			gridItemsCurrent = (GameObject)GameObject.Instantiate (Resources.Load ("Prefabs/Caracteristicas/GridCaracteristicasYanaconas"));			
			// Dado que Unity le cambia el nombre al GameObject creado como copia de un Prefab, le asignamos el nombre original
			gridItemsCurrent.name = "Yanaconas";			
			// Lo agregamos como hijo del HUD
			gridItemsCurrent.transform.parent = parentGrids.transform;		
			// La asignaciom modifica el Transform, asi que lo reiniciamos
			gridItemsCurrent.transform.localPosition = new Vector3(-308.5389f, -150, 0);
			gridItemsCurrent.transform.localRotation = Quaternion.identity;
			gridItemsCurrent.transform.localScale = Vector3.one;
			
			scrollView.ResetPosition();
			scrollBar.ForceUpdate();
			
			//CambiarColor(4);
			//			gridItemsCurrent.GetComponent<UIGrid>().repositionNow = true;
			//			gridItemsCurrent.GetComponent<UIGrid>().Reposition();
			break;
			default:
			break;
		}


	}

	void Update()
	{

		scrollView.UpdatePosition();
	}

}
