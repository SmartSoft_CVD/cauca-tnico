﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowController : MonoBehaviour {

	public GameObject [] arrows;
	public UICenterOnChild grid;
	public GameObject containerPoints;

	public List <GameObject> itemsOfGrid;
	public List <GameObject> points;

	GameObject objCentered;


	// Use this for initialization
	void Start ()
	{
		foreach (Transform item in grid.transform)
		{
			itemsOfGrid.Add(item.gameObject);
		}

		foreach (Transform item in containerPoints.transform)
		{
			points.Add(item.gameObject);
		}

		NGUITools.SetActive(arrows[1], false);
		grid.onFinished += MetodoOnDrag;
	}

	void MetodoOnDrag()
	{
		objCentered = grid.centeredObject;

		for (int d = 0; d < itemsOfGrid.Count; d++)
		{
			if(objCentered.Equals(itemsOfGrid[d]))
			{
				points[d].SendMessage("OnClick");

			}
		}


		
		if(objCentered.Equals(itemsOfGrid[0]))
		{
			NGUITools.SetActive(arrows[0], true);
			NGUITools.SetActive(arrows[1], false);
		}
		else if(objCentered.Equals(itemsOfGrid[itemsOfGrid.Count - 1]))
		{
			NGUITools.SetActive(arrows[0], false);
			NGUITools.SetActive(arrows[1], true);
		}
		else
		{
			NGUITools.SetActive(arrows[0], true);
			NGUITools.SetActive(arrows[1], true);
		}
	}
}
