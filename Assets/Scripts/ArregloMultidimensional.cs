using UnityEngine;
using System.Collections;

[System.Serializable]
public class ArregloMultidimensional
{
	public string [] stringArreglo = new string[0];

	public string this[int index]{
		get {
			return stringArreglo[index];
		}
		set {
			stringArreglo[index] = value;
		}
	}
	
	public int Length {
        get {
            return stringArreglo.Length;
        }
    }
 
    public long LongLength {
        get {
            return stringArreglo.LongLength;
        }
    }
	
}
