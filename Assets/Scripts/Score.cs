﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public UILabel labelScore;
	public UILabel labelPuntaje;
	public int scoreCurrent = 0;

	public UILabel scoreFacebook;
	public UILabel lvlFacebook;

	int totalScore;

	string textoDeNivel;

	// Use this for initialization
	void Start ()
	{
		labelScore.text = "0";


	}
	
	// Update is called once per frame
	void Update ()
	{
		textoDeNivel = PlayerPrefs.GetString("ModeGame");
		
		if(textoDeNivel == "dificil")
			lvlFacebook.text = "difícil";
		
		if(textoDeNivel == "facil")
			lvlFacebook.text = "fácil";
	
	}

	public void SetScore(int scoreObtained)
	{
		scoreCurrent += scoreObtained;
		labelScore.text = scoreCurrent.ToString();
		labelPuntaje.text = "Tu Puntaje: " + scoreCurrent.ToString();

		scoreFacebook.text = scoreCurrent.ToString();
	}
}
