﻿using UnityEngine;

[RequireComponent(typeof(UIWidget))]
public class ColorWrapper : MonoBehaviour
{
	public Color color = Color.white;
	private GameObject parentColors;
	private ColorsCollection parenColorsComp;

	private int indiceColor;
	
	UIWidget mWidget;
	
	void Awake ()
	{
//		mWidget = GetComponent<UIWidget>();
//
//		parentColors = GameObject.FindWithTag("Customizar");
//		parenColorsComp = parentColors.GetComponent<SelectionItemsRegion>();
//		indiceColor = PlayerPrefs.GetInt("ColorAvatar");
	}

	void Start()
	{
		mWidget = GetComponent<UIWidget>();
		
		parentColors = GameObject.FindWithTag("ColorCollection");
		parenColorsComp = parentColors.GetComponent<ColorsCollection>();
		indiceColor = PlayerPrefs.GetInt("ColorAvatar");
	}
	
	void Update ()
	{
		mWidget.color = parenColorsComp.coloresSprites[indiceColor];
	}
}