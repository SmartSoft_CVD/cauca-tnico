﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LogicaJuego : MonoBehaviour {

	private bool oneTimePowerUp = false;

	public bool OneTimePowerUp {
		get {
			return oneTimePowerUp;
		}
		set {
			oneTimePowerUp = value;
		}
	}

	private bool oneTimePowerUpFlip = false;

	public bool OneTimePowerUpFlip {
		get {
			return oneTimePowerUpFlip;
		}
		set {
			oneTimePowerUpFlip = value;
		}
	}

	public GameObject timeDialog;
	public GameObject EyeDialog;
	public GameObject cameraNGUI;

	public AudioClip [] clipsDontMatching;
	public AudioClip clipMatching;
	public AudioClip clipPassLevel;
	public AudioClip clipEndGame;
	public AudioClip clipBonusTime;

	public TweenScale effectTimeBonus;
	public TweenScale effectAddScore;


	private IntanciacionTarjetas[] cards;

	public TweenPosition tweenScoreContainer;

	public IntanciacionTarjetas[] Cards {
		get {
			return cards;
		}
		set {
			cards = value;
		}
	}

	public GameObject parentCards;

	private int setsofcards;
	public int nroftries = 0;

	Bajarar barajarComp;

	public int numeroDeParejasConsecutivas = 0;

	public Score score;
	public CountDown countDown;

	public TweenPosition[] tweensHud;

	public GameObject tweensButtonRestart;
	public GameObject tweensButtonFacebook;

	public List<GameObject> cardsInstantiated;
	public GameObject coin;
	private GameObject coinInstantiated;

	#region Objetos power up flip
	public GameObject prefabFlip;
	private GameObject prefabFlipInstantiated;
	#endregion

	[SerializeField]
	public BetterList<string> listaSprites;
	public UIAtlas atlasTest;

	public static bool terminoJuego = false;

	private bool realizoPowerUpFlip = false;

	public bool RealizoPowerUpFlip {
		get {
			return realizoPowerUpFlip;
		}
		set {
			realizoPowerUpFlip = value;
		}
	}

	private bool oneTimeSoundLose = false;
	bool pausedGame;



	// Use this for initialization
	void Start ()
	{


		oneTimeSoundLose = false;
		listaSprites  =  atlasTest.GetListOfSprites();

		terminoJuego = false;
//		Debug.LogWarning("Numero de elementos de la lista de sprite: " + listaSprites.size);

		barajarComp = this.gameObject.GetComponent<Bajarar>();

		cards = new IntanciacionTarjetas[2];

		UIButton compButton;
		compButton = tweensButtonRestart.GetComponent<UIButton>();
		EventDelegate.Add(compButton.onClick, delegate() {

			Application.LoadLevel(0);
		});

		cardsInstantiated = new List<GameObject>();

		EventDelegate.Add(tweenScoreContainer.onFinished, delegate()
		                  { terminoJuego = true; });
	}

	void PlaySoundRandom()
	{
		if(audio.isPlaying) return;
		audio.clip = clipsDontMatching[Random.Range(0, clipsDontMatching.Length)];
		audio.Play();
	}


	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.V))
		{


		}
		//Debug.Log("sss: " + setsofcards);


		// Si se termino el tiempo

		if(countDown.t <= 0.0f)
		{
			countDown.t = 0;

			foreach (Transform cardCurrent in parentCards.gameObject.transform)
			{
				cardCurrent.GetComponent<BoxCollider>().enabled = false;				
			}

			this.gameObject.GetComponent<TweenPosition>().PlayForward();

			foreach (TweenPosition tweenCurrent in tweensHud)
			{
				tweenCurrent.PlayReverse();

			}


//			tweensButtonRestart.transform.parent.GetComponent<TweenPosition>().PlayForward();
//			tweensButtonFacebook.transform.parent.GetComponent<TweenPosition>().PlayForward();

			foreach (Transform cardCurrent in parentCards.gameObject.transform)
			{
				NGUITools.SetActive(cardCurrent.gameObject, false);			
			}

			tweenScoreContainer.PlayForward();



			if(!oneTimeSoundLose)
			{
				audio.PlayOneShot(clipEndGame);
				oneTimeSoundLose = true;
			}


		}
	}
	
	public void CheckCards(IntanciacionTarjetas mc){
		if(cards[0] == null)
			cards[0] = mc;
		else{
			cards[1] = mc;
			nroftries++;

			foreach (Transform cardCurrent in parentCards.gameObject.transform)
			{
				cardCurrent.GetComponent<UIButton>().isEnabled = false;				
			}

			if(cards[0].cardnumber == cards[1].cardnumber)
			{
				audio.PlayOneShot(clipMatching);



				barajarComp.numeroDeCartas.Clear();
				foreach (Transform oo in parentCards.transform)
				{
					
					barajarComp.numeroDeCartas.Add(oo);
					
					
				}

				if(cards[0].tag == "PowerUp" || cards[1].tag == "PowerUp")
				{

					Debug.Log("Sumar power up");
					foreach (IntanciacionTarjetas cardWithTag in cards)
					{
						if(cardWithTag.tag == "PowerUp")
						{

							int tiempoDado = (int)cardWithTag.transform.FindChild("Coin(Clone)").GetComponent<TimeBonus>().timeBonus;
							Debug.Log("Tiempo dado: " + tiempoDado);
							countDown.t = countDown.t + tiempoDado;
							audio.PlayOneShot(clipBonusTime);
							effectTimeBonus.PlayForward();
							barajarComp.executePowerUp = false;


						}
						
					}
				}

				if(cards[0].tag == "PowerUpFlip" || cards[1].tag == "PowerUpFlip")
				{
					
					Debug.Log("Sumar power up flip");
					foreach (IntanciacionTarjetas cardWithTag in cards)
					{
						if(cardWithTag.tag == "PowerUpFlip")
						{

							if(barajarComp.numeroDeCartas.Count != 4 && barajarComp.numeroDeCartas.Count > 4)
							{	realizoPowerUpFlip = true;
								StartCoroutine(barajarComp.PowerUpDestapar());
							}
							else
							{
								// destruir el item powerup
								//Destroy((cardWithTag.transform.FindChild("Coin(Clone)").GetComponent)
								
								foreach (Transform cardCurrent in parentCards.gameObject.transform)
								{
									cardCurrent.GetComponent<UIButton>().isEnabled = true;				
								}
								cardWithTag.tag = "Untagged";
								realizoPowerUpFlip = false;
								//TODO
								barajarComp.executePowerUpFlip = false;
								Debug.LogError("--------------------------------------------------------------------------------------------------- No debe de hacer nada");
							}

						}
						
					}
				}


				StartCoroutine(CardsMatching());
				numeroDeParejasConsecutivas+=1;

				if(numeroDeParejasConsecutivas > 1)
				{
					effectAddScore.PlayForward();
					score.SetScore(40 * numeroDeParejasConsecutivas);
				}
				else
				{
					effectAddScore.PlayForward();
					score.SetScore(40);
				}
			}
			else
			{
				Handheld.Vibrate();

				// Nuevo código corrección de Bug
				#region Nuevo código corrección de Bug
				if(cards[0].tag == "PowerUpFlip" || cards[1].tag == "PowerUpFlip")
				{
					Debug.Log("<......................... Esta carta NO es la pareja, por consiguiente aquí debería eliminarse .........................>");

					GameObject cardWithTag = GameObject.FindGameObjectWithTag("PowerUpFlip");
					cardWithTag.tag = "Untagged";

					barajarComp.executePowerUpFlip = false;
					Transform itemFlip = cardWithTag.transform.FindChild("PrefabFlip(Clone)");
					
					Destroy(itemFlip.gameObject);

				}
				#endregion

				audio.PlayOneShot(clipsDontMatching[0]);
				CardsNotMatching();
				numeroDeParejasConsecutivas = 0;
			}





//			cards[0] = null;
//			cards[1] = null;
		}
	}

	public void RandomPowerUp()
	{
		barajarComp.executePowerUp = true;
		foreach (Transform cardCurrent in parentCards.gameObject.transform)
		{
			cardsInstantiated.Add(cardCurrent.gameObject);
		}

		int numberRandom = Random.Range(1, cardsInstantiated.Count - 1);

		Debug.LogWarning("1. Se ejecuto el powerup");

		if(cardsInstantiated[numberRandom] != null)
		{
			Debug.LogWarning("2. Se ejecuto el powerup");

			if(cardsInstantiated[numberRandom].gameObject.tag != "PowerUp" && cardsInstantiated[numberRandom].gameObject.tag != "PowerUpFlip")
			{
				cardsInstantiated[numberRandom].gameObject.tag = "PowerUp";
				// Luego de tener una de la carta con el tag PowerUp instanciamos un sprite
				coinInstantiated = NGUITools.AddChild(cardsInstantiated[numberRandom].gameObject, coin);
				coinInstantiated.transform.localPosition = Vector3.zero;


				coinInstantiated.transform.FindChild("TimeClock").GetComponent<UISprite>().width = (int)barajarComp.TamanoCasillas / 2;
				coinInstantiated.transform.FindChild("TimeClock").GetComponent<UISprite>().height = (int)barajarComp.TamanoCasillas / 2	;

				coinInstantiated.transform.FindChild("Bck").GetComponent<UISprite>().width = (int)barajarComp.TamanoCasillas / 2;
				coinInstantiated.transform.FindChild("Bck").GetComponent<UISprite>().height = (int)barajarComp.TamanoCasillas / 2	;
			}



		}
		Debug.LogWarning("Se ejecuto el powerup");


	}


	public void RandomPowerUpFlip()
	{


		barajarComp.executePowerUpFlip = true;

		foreach (Transform cardCurrent in parentCards.gameObject.transform)
		{
			cardsInstantiated.Add(cardCurrent.gameObject);
		}
		
		int numberRandom = Random.Range(1, cardsInstantiated.Count - 1);
		
		if(cardsInstantiated[numberRandom] != null)
		{
			if(cardsInstantiated[numberRandom].gameObject.tag != "PowerUpFlip" && cardsInstantiated[numberRandom].gameObject.tag != "PowerUp")
			{
				cardsInstantiated[numberRandom].gameObject.tag = "PowerUpFlip";
				// Luego de tener una de la carta con el tag PowerUp instanciamos un sprite
				prefabFlipInstantiated = NGUITools.AddChild(cardsInstantiated[numberRandom].gameObject, prefabFlip);
				prefabFlipInstantiated.transform.localPosition = new Vector3(0, 0, 10.0f);


				
	//			prefabFlipInstantiated.transform.GetComponent<UISprite>().width = (int)barajarComp.TamanoCasillas / 2;
	//			prefabFlipInstantiated.transform.GetComponent<UISprite>().height = (int)barajarComp.TamanoCasillas / 2;

	//			Vector3 sizeFlip = prefabFlipInstantiated.transform.localScale;
	//			sizeFlip = new Vector3((int)barajarComp.TamanoCasillas / 2, (int)barajarComp.TamanoCasillas / 2, 10.0f);
	//			prefabFlipInstantiated.transform.localScale = sizeFlip;

				prefabFlipInstantiated.transform.GetComponent<UISprite>().width = (int)barajarComp.TamanoCasillas / 2;
				prefabFlipInstantiated.transform.GetComponent<UISprite>().height = (int)barajarComp.TamanoCasillas / 2;
			}
			

			
		}
		Debug.LogWarning("Se ejecuto el powerup flip");
		//barajarComp.executePowerUpFlip = false;

	}
//
//
//			if(timeBonusFill <= 0)
//			{
//				Destroy(coinInstantiated);
//				barajarComp.executePowerUp = false;
//			}





//			coin.transform.localScale barajarComp.TamanoCasillas/2;
//
//			coin.transform.FindChild("Front").GetComponent<UISprite>().width
		

		
	

	void QuitTag()
	{
		foreach (GameObject cardCurrent in cardsInstantiated)
		{
			if(cardCurrent.tag == "PowerUp")
				cardCurrent.tag = "";
		}
	}



	
	IEnumerator CardsMatching()
	{


		yield return new WaitForEndOfFrame();

		cards[0].RemoveCard();
		cards[1].RemoveCard();




		yield return new WaitForSeconds(0.5f);

		if(!realizoPowerUpFlip)
		{
			foreach (Transform cardCurrent in parentCards.gameObject.transform)
			{
				cardCurrent.GetComponent<UIButton>().isEnabled = true;
			}
		}

		
		setsofcards--;
		if(setsofcards == 0)
			StartCoroutine(GameEnd());
	}
	void CardsNotMatching(){

		//PlaySoundRandom();
		cards[0].Hide();
		cards[1].Hide();
	}
	IEnumerator GameEnd(){
		Debug.Log("Game has ended, number of tries: " + nroftries);

		cardsInstantiated.Clear();

		barajarComp.cardslist.Clear();

		yield return new WaitForSeconds(0.6f);
		numeroDeParejasConsecutivas = 0;

		if(countDown.t > 0.0f){

			audio.PlayOneShot(clipPassLevel);
			barajarComp.Iniciar();

		}
	}

	public void SetSetsOfCards(int i){
		setsofcards = i;
	}
}
