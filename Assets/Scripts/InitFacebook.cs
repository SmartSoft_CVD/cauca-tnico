﻿using UnityEngine;
using System.Collections;

public class InitFacebook : MonoBehaviour {

	void Awake()
	{
		DontDestroyOnLoad (this.transform.gameObject);
		FB.Init (OnInitComplete, OnHideUnity);
	}
	
	private void OnInitComplete ()
	{
		Debug.Log ("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
		enabled = true;
	}
	
	//Called when the SDk tries to display HTML content
	private void OnHideUnity(bool isGameShown){
		if(!isGameShown){
			Time.timeScale = 0;
		} else{
			Time.timeScale = 1;
		}
	}
}
