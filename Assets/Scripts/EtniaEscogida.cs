﻿using UnityEngine;
using System.Collections;

public class EtniaEscogida : MonoBehaviour {

	private string region;
	private string regionTexto;
	// Use this for initialization
	void Start ()
	{
		region = PlayerPrefs.GetString("RegionActual");

		switch (region)
		{
			case "Campesinos":
			regionTexto = "Campesinos";
				break;

		case "Afro":
			regionTexto = "AfroColombianos";
			break;

		case "Guambianos":
			regionTexto = "Guambianos";
			break;

		case "Nasa":
			regionTexto = "Nasa";
			break;

		case "Yanaconas":
			regionTexto = "Yanaconas";
			break;

				default:
						break;
		}
	
		this.gameObject.GetComponent<UILabel>().text = regionTexto;
	}
}
