﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum States { STATEDIALOG, STATECUSTOMIZE };
public class SplitClass : MonoBehaviour
{
	private States currentState;
	public Collider bckCollider;

	string caracteristica;
	string numeroCaracteristica;

	private string rootGridOne = "UI Root/Camera/HUD/Caracteristicas/OffsetCaracteristicas/ScrollViewOne/GridOne";
	private string rootGridTwo = "UI Root/Camera/HUD/Caracteristicas/OffsetCaracteristicas/ScrollViewTwo/GridTwo";
	private string rootGridThree = "UI Root/Camera/HUD/Caracteristicas/OffsetCaracteristicas/ScrollViewThree/GridThree";

	private string rootDialog = "UI Root/Camera/HUD/Caracteristicas/OffsetCaracteristicas/DialogSprite";
	private string rootCloseDialog = "UI Root/Camera/HUD/Caracteristicas/OffsetCaracteristicas/DialogSprite/ButtonCloseDialog";
	private string rootAvatar = "UI Root/Camera/HUD/Avatar/OffsetAvatar";

	string [] partsOfName;
	string nameComplete;

	public UILabel [] labelSprite;
	public UISprite [] sprite;

	private GameObject parentGridOne;
	private GameObject parentGridTwo;
	private GameObject parentGridThree;


	private GameObject parentDialog;
	private GameObject parentCloseDialog;
	private GameObject parentAvatar;

	private UIButton compCloseDialog;
	UICenterOnChild objectCenteredOne;
	UICenterOnChild objectCenteredTwo;
	UICenterOnChild objectCenteredThree;

	public List<GameObject> ItemsList;
	public List<GameObject> partsAvatar;

	public UILabel textDialog;

	public string [] listTextsDialogR;
	public string [] listTextsDialogC;
	public string [] listTextsDialogP;
	
	// Use this for initialization
	void Start ()
	{
		currentState = States.STATECUSTOMIZE;
		bckCollider.enabled = false;

		for (int i = 0; i < labelSprite.Length; i++)
		{
			labelSprite[i].text = "0";
		}

		parentGridOne = GameObject.Find(rootGridOne);
		parentGridTwo = GameObject.Find(rootGridTwo);
		parentGridThree = GameObject.Find(rootGridThree);

		parentDialog = GameObject.Find(rootDialog);
		parentCloseDialog = GameObject.Find(rootCloseDialog);

		parentAvatar = GameObject.Find(rootAvatar);

		compCloseDialog = parentCloseDialog.GetComponent<UIButton>();

		EventDelegate.Add(compCloseDialog.onClick, delegate() {
					parentDialog.GetComponent<TweenScale>().PlayReverse();

			if(parentDialog.GetComponent<TweenScale>().direction == AnimationOrTween.Direction.Reverse)
			{
				ChangeCurrentState(States.STATECUSTOMIZE, false);
			}



		});

		ItemsList = new List<GameObject>();

		foreach (Transform itemCurrent in parentGridOne.transform) 
		{
			ItemsList.Add(itemCurrent.gameObject);
			UIEventListener.Get(itemCurrent.gameObject).onClick += GetNameWidget;

		}

		foreach (Transform itemCurrent in parentGridTwo.transform) 
		{
			UIEventListener.Get(itemCurrent.gameObject).onClick += GetNameWidget;
		}

		foreach (Transform itemCurrent in parentGridThree.transform) 
		{
			UIEventListener.Get(itemCurrent.gameObject).onClick += GetNameWidget;
			
		}



		#region
		partsAvatar = new List<GameObject>();
		
		foreach (Transform currentPart in parentAvatar.transform)
		{
			partsAvatar.Add(currentPart.gameObject);
		}

		partsAvatar[0].GetComponent<ShowHideGrids>().activate = parentGridOne;
		partsAvatar[0].GetComponent<ShowHideGrids>().desactivate[0] = parentGridTwo;
		partsAvatar[0].GetComponent<ShowHideGrids>().desactivate[1] = parentGridThree;
		
		partsAvatar[0].GetComponent<ShowHideGrids>().Function();

		ShowHideGrids.ShowCurrentScrollView(partsAvatar[0], parentGridOne, parentGridTwo, parentGridThree);
		ShowHideGrids.ShowCurrentScrollView(partsAvatar[1], parentGridThree, parentGridTwo, parentGridOne);
		ShowHideGrids.ShowCurrentScrollView(partsAvatar[2], parentGridTwo, parentGridOne, parentGridThree);


		#endregion

		objectCenteredOne = parentGridOne.GetComponent<UICenterOnChild>();
		objectCenteredTwo = parentGridTwo.GetComponent<UICenterOnChild>();
		objectCenteredThree = parentGridThree.GetComponent<UICenterOnChild>();

	}

	public void ChangeCurrentState(States state, bool bckIsEnabled)
	{
		EventDelegate.Add(parentDialog.GetComponent<TweenScale>().onFinished, delegate() { currentState = state; });
		bckCollider.enabled = bckIsEnabled;
	}


	public void Update()
	{
		partsAvatar[0].transform.GetChild(0).GetComponent<UISprite>().spriteName = objectCenteredOne.centeredObject.name;
		partsAvatar[1].transform.GetChild(0).GetComponent<UISprite>().spriteName = objectCenteredThree.centeredObject.name;
		partsAvatar[2].transform.GetChild(0).GetComponent<UISprite>().spriteName = objectCenteredTwo.centeredObject.name;

		Debug.Log("Estado actual: " + currentState);
	}

	public void GetNameWidget(GameObject gO)
	{
		parentDialog.GetComponent<TweenScale>().PlayForward();

		if(parentDialog.GetComponent<TweenScale>().direction == AnimationOrTween.Direction.Forward)
		{
			ChangeCurrentState(States.STATEDIALOG, true);
		}

		nameComplete = gO.name;
		partsOfName = nameComplete.Split(new char[]{ '_' });

		numeroCaracteristica = partsOfName[1];

		switch (partsOfName[0])
		{
			case "R":
				textDialog.text = listTextsDialogR[int.Parse(numeroCaracteristica)];				
				break;

			case "C":
				textDialog.text = listTextsDialogC[int.Parse(numeroCaracteristica)];				
				break;

			case "P":
				textDialog.text = listTextsDialogP[int.Parse(numeroCaracteristica)];				
				break;

			default:
				break;
		}


	}
}
