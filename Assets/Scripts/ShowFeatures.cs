﻿using UnityEngine;
using System.Collections;

public class ShowFeatures : MonoBehaviour
{
	public GameObject [] elementos;
	public UISprite [] sprites;
	public UILabel [] textos;

	public UIButton buttonAvatar;
	public static bool ocultarMostrar = true;

	public UILabel textoRegion;

	// Use this for initialization
	void Start ()
	{
		ocultarMostrar = true;

		string [] imagesRecieved = PlayerPrefsX.GetStringArray("ImagesFeatures");

		for (int d = 0; d < imagesRecieved.Length; d++)
		{
			sprites[d].spriteName = imagesRecieved[d];
		}

		string [] messagesRecieved = PlayerPrefsX.GetStringArray("Features");

		for (int i = 0; i < messagesRecieved.Length; i++)
		{
			textos[i].text = messagesRecieved[i];
		}


		string textoDeLaRegion = PlayerPrefs.GetString("RegionActual");

		switch (textoDeLaRegion)
		{
			case "Afro":
				textoRegion.text =  "¡Soy Afrocolombiano(a)!"; 
				break;

			case "Campesinos":
				textoRegion.text =  "¡Soy Campesino(a)!"; 
				break;

			case "Guambianos":
				textoRegion.text =  "¡Soy Guambiano(a)!"; 
				break;

			case "Nasa":
				textoRegion.text =  "¡Soy Nasa!"; 
				break;

			case "Yanaconas":
				textoRegion.text =  "¡Soy Yanacona!"; 
				break;

				default:
						break;
		}

	

	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!ocultarMostrar)
		{
			NGUITools.SetActive(elementos[0], true);
			NGUITools.SetActive(elementos[1], false);
		}
		else
		{
			NGUITools.SetActive(elementos[0], false);
			NGUITools.SetActive(elementos[1], true);
		}
	
	}
}
