﻿using UnityEngine;
using System.Collections;

public class FBFriend : MonoBehaviour {

	public UILabel scoreLbl;
	public UILabel rankLbl;
	public UITexture texture;
	public int score;
	public int rank;
	public string pictureUrl;
	public string id;
	public UILabel nameLbl;

	private string root = "UI RootFixedOnMobiles/Camera/ScoreContainer/BckBlue/ScrollView";

	private UIDragScrollView dragScrollView;
	private UIScrollView scrollViewObj;

	private bool beated = false;

	void Awake()
	{
		dragScrollView = this.gameObject.GetComponent<UIDragScrollView>();
		scrollViewObj = GameObject.Find(root).GetComponent<UIScrollView>();


	}

	// Use this for initialization
	void Start ()
	{
		if(dragScrollView != null)
			dragScrollView.scrollView = scrollViewObj;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setData(int score, int rank, string url, string id, string name){
		this.score = score;
		this.pictureUrl = url;
		this.rank = rank;
		this.id = id;
		//nameLbl.text = name.ToString();
		scoreLbl.text = score.ToString();
		rankLbl.text = this.rank.ToString();
		if(this.pictureUrl != null && !"".Equals(this.pictureUrl)){
			StartCoroutine(loadPicture());
		}
	}

	IEnumerator loadPicture(){
		WWW www = new WWW(this.pictureUrl);
		yield return www;
		/*UITexture ut = NGUITools.AddWidget<UITexture>(gameObject);
		ut.material = new Material(Shader.Find("Unlit/Transparent Colored"));
		ut.material.mainTexture = www.texture;
		ut.transform.localPosition = this.texture.transform.localPosition;
		ut.depth = this.texture.depth;
		ut.transform.localRotation = this.texture.transform.localRotation;

		Destroy(this.texture);*/
		this.texture.mainTexture = www.texture;
	}

	public void Beat(){
		if(!beated){
			beated = true;
			TweenRotation tr = TweenRotation.Begin(gameObject, 0.1f, Quaternion.Euler(new Vector3(0f,0f,1f)));
			tr.from = new Vector3(0f,0f,-1.0f);
			tr.style = UITweener.Style.PingPong;
		}
	}


}
