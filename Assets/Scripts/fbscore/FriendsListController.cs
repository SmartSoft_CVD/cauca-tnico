﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendsListController : MonoBehaviour {

	public GameObject friendPrefab;
	public GameObject listSeparatorPrefab;

	public List<FBFriend> friendList;
	public UILabel textLabel;

	public string emptyListMessageName;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetList(string listjson){

//		string filename = "Strings/LoadingStrings";
//		TextAsset bindata = Resources.Load(filename) as TextAsset;
//		if(bindata == null){
//			Debug.LogError("Imposible load file "+filename+" be sure the file exists in the correct directory!");
//		}
//		
		//SimpleJSON.JSONNode node = SimpleJSON.JSON.Parse(bindata.text);
		
//		string emptymessage = node[emptyListMessageName];

		UIGrid table = GetComponent<UIGrid>();


		SimpleJSON.JSONNode node = SimpleJSON.JSON.Parse(listjson);

		if(node["error"] != null){
			return;
		}

		SimpleJSON.JSONArray friends = node["data"].AsArray;

		if(friends != null && friends.Count > 0){
//			Destroy (textLabel);
			for(int i = 0; i < friends.Count; i++){
				LoadFriend(friends[i], "friend"+i);
			}
			GetComponent<UIGrid>().repositionNow = true;
		}else if(friends.Count <= 0){
//			textLabel.text = emptymessage;
			Debug.Log("eres el primero de tus amigos en jugar");
		}

	}

	void LoadFriend(SimpleJSON.JSONNode f, string name){
		string prefName = "Prefabs/FBFriends/Friend";
		//GameObject go = (GameObject)Instantiate(Resources.Load(prefName,typeof(GameObject)));

		if(f["separator"].AsBool){
			GameObject sep = NGUITools.AddChild(gameObject, listSeparatorPrefab);
			sep.name = name;
			return;
		}

		GameObject go = NGUITools.AddChild(gameObject, friendPrefab);
		go.name = name;
		Destroy(go.GetComponent<UIPanel>());
		FBFriend fbFriend = go.GetComponent<FBFriend>();
		friendList.Add (fbFriend);
		fbFriend.setData(f["score"].AsInt,f["rank"].AsInt,f["picture"],f["id"],f["name"]);

	}

	public void SetText(string text){
		textLabel.text = text;
	}
}
