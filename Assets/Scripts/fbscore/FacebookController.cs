﻿using UnityEngine;
using System.Collections;

public class FacebookController : MonoBehaviour {

	public GameObject buttonFacebook;
	private UIButton compButtonFacebook;
	public FriendsListController friendLisController;
	public Score score;
	public ToggleTime toggleComp;
	int gottenScore;

	string currentLevelName;

	public GameObject [] switchLogged;

	void Awake()
	{
		compButtonFacebook = buttonFacebook.GetComponent<UIButton>();


	}

	void Start()
	{
		if(FB.IsLoggedIn)
		{

		}



		EventDelegate.Add(compButtonFacebook.onClick, delegate ()
  		{
			if(!FB.IsLoggedIn)
			{
				CallFBLogin();
			}
			else
			{
				// Lanzar método
				StartCoroutine(SendScoreToServer());
			}

		});


	}

	void Update()
	{
//		if(LogicaJuego.terminoJuego)
//		{
//			StartCoroutine(SendScoreToServer());
//		}

		if(!FB.IsLoggedIn)
		{
			NGUITools.SetActive(switchLogged[0], true);
			NGUITools.SetActive(switchLogged[1], false);
		}
		else
		{
			NGUITools.SetActive(switchLogged[0], false);
			NGUITools.SetActive(switchLogged[1], true);

			if(LogicaJuego.terminoJuego)
			{
				LogicaJuego.terminoJuego = false;
				StartCoroutine(SendScoreToServer());

			}
			
		}
	}

	private void CallFBLogin ()
	{
		FB.Login ("email,publish_actions", LoginCallback);
	}

	void LoginCallback (FBResult result)
	{
		if(result != null)
		{
			Debug.LogError("Error: " + result.Error);
		}else
		{
			StartCoroutine(SendScoreToServer());
		}

	}

	IEnumerator GetFriendsList()
	{
		
		string serverip = "http://54.86.194.167/scores/";
		//currentLevelName = toggleComp.levelCurrent.ToString();
		string url ="http://"+serverip+"GetFriendsScores.php?level="+PlayerPrefs.GetString("ModeGame")+"&accessToken="+FB.AccessToken+"&idapp="+FB.AppId;

		Debug.Log("Solicitando ---------> "+url);
		WWW www = new WWW(url);
		yield return www;
		string friends = www.text;
		Debug.Log("********Este es el listado de amigos ---->"+friends);

		friendLisController.SetList(friends);

	}

	IEnumerator SendScoreToServer(){

		

		string serverip = "http://54.86.194.167/scores/";



		gottenScore = score.scoreCurrent;
		Debug.Log("1. --------------------------------------------> Puntaje Actual: " + gottenScore + " Puntaje Anterior: " + PlayerPrefs.GetInt("Score"));

		if(PlayerPrefs.GetInt("Score") < gottenScore)
			PlayerPrefs.SetInt("Score", gottenScore);


		Debug.Log("2. --------------------------------------------> Puntaje Actual: " + gottenScore + " Puntaje Anterior: " + PlayerPrefs.GetInt("Score"));

		//currentLevelName = toggleComp.levelCurrent.ToString();

		Debug.Log("--------------------------------------------> este es el resultado del enum: " + PlayerPrefs.GetString("ModeGame"));
		Debug.Log("Este es el nivel: " + PlayerPrefs.GetString("ModeGame"));
		
		string url ="http://"+serverip+"AddScore.php?level="+PlayerPrefs.GetString("ModeGame")+"&score="+PlayerPrefs.GetInt("Score")+"&accessToken="+FB.AccessToken+"&idapp="+FB.AppId;
		Debug.Log("Solicitando ---------> "+url);
		WWW www = new WWW(url);
		yield return www;
		string result = www.text;
		Debug.Log("********Este es el resultado del env’o de score  ---->"+result);

		SimpleJSON.JSONNode node = SimpleJSON.JSON.Parse(result);
		if(node["error"] == null){
			StartCoroutine(GetFriendsList());
		}

		
	}


	
}
