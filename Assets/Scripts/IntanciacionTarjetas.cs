﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IntanciacionTarjetas : MonoBehaviour
{
	public AudioClip [] clipsShow;

	public int cardnumber;
	private bool selected = false;

	private GameObject lJObj;
	private LogicaJuego lJScript;
	private Bajarar BScript;

	public RuntimeAnimatorController controlCarta_1;
	public RuntimeAnimatorController controlCarta_2;

	private GameObject parentCards;
	public bool comprobarCarta = false;

	/* -------------------------- */
	TweenRotation tweenPrimeraCartaFrontal;
	/* -------------------------- */

	public bool frente;
	public bool voltear;

	public Object[] myTextures;


	void Start()
	{
		lJObj = GameObject.Find("Barajar");
		lJScript = lJObj.GetComponent<LogicaJuego>();
		BScript = lJObj.GetComponent<Bajarar>();


		parentCards = GameObject.Find("Espacio2");


	
	}

	void PlaySoundRandom()
	{
		if(audio.isPlaying) return;
		audio.clip = clipsShow[Random.Range(0, clipsShow.Length)];
		audio.Play();
	}

	public void SetMemorycard(string t, int number)
	{
//		this.transform.FindChild("F").gameObject.GetComponent<UISprite>().spriteName = t;


		myTextures = Resources.LoadAll<Sprite>("IconosCartas");

		for (int x = 0; x < myTextures.Length; x++)
		{
			//Debug.LogWarning(myTextures[x].name);
		}

		this.transform.FindChild("F").gameObject.GetComponent<SpriteRenderer>().sprite = (Sprite)myTextures[int.Parse(t) - 1];

		cardnumber = number;
	}

	//Este método anima la tarjeta para que se voltee
	// lo que hacemos cuando presioamos la tarjeta
	public void Show(){
		if(!selected){
			selected = true;

			/* Hasta aquí voltea la carta */
			MostrarCarta(this.gameObject);

			PlaySoundRandom();

			if(comprobarCarta)
			{


				if(this.gameObject.tag == "PowerUp" && !lJScript.OneTimePowerUp)
				{
					lJScript.OneTimePowerUp = true;
					GameObject dialogoTiempo = NGUITools.AddChild(lJScript.cameraNGUI, lJScript.timeDialog);
					dialogoTiempo.transform.localPosition = new Vector3(0, 1000f, 0);
					
					dialogoTiempo.GetComponent<TweenPosition>().Play();
					
					
					EventDelegate.Add(dialogoTiempo.GetComponent<TweenPosition>().onFinished, delegate()
					                  { dialogoTiempo.GetComponent<TweenColor>().Play(); dialogoTiempo.GetComponent<TweenPosition>().delay = 1.0f; dialogoTiempo.GetComponent<TweenPosition>().duration = 6.0f; dialogoTiempo.GetComponent<TweenPosition>().PlayReverse(); });
					
					EventDelegate.Add(dialogoTiempo.GetComponent<TweenColor>().onFinished, delegate()
					                  {	Destroy(dialogoTiempo.gameObject); });
				}

			

				if(this.gameObject.tag == "PowerUpFlip" && !lJScript.OneTimePowerUpFlip)
				{
					lJScript.OneTimePowerUpFlip = true;
					GameObject dialogoOjo = NGUITools.AddChild(lJScript.cameraNGUI, lJScript.EyeDialog);
					dialogoOjo.transform.localPosition = Vector3.zero;
					
					dialogoOjo.GetComponent<TweenPosition>().Play();
					
					
					EventDelegate.Add(dialogoOjo.GetComponent<TweenPosition>().onFinished, delegate()
					                  { dialogoOjo.GetComponent<TweenColor>().Play(); dialogoOjo.GetComponent<TweenPosition>().delay = 1.0f; dialogoOjo.GetComponent<TweenPosition>().duration = 6.0f; dialogoOjo.GetComponent<TweenPosition>().PlayReverse();});
					
					EventDelegate.Add(dialogoOjo.GetComponent<TweenColor>().onFinished, delegate()
					                  {	Destroy(dialogoOjo.gameObject); });
				}
			// Comprobar luego de ejecutar la animación
				lJScript.CheckCards(this);
			}
		}
	}

	// Este método sirve para esconder la carta
	public void Hide(){



		StartCoroutine(MetodoAnim());
		/* Hasta aquí oculta la carta */
		OcultarCarta(this.gameObject);



		selected = false;
	}

	public void OcultarCarta(GameObject gO)
	{
//		if(frente)
//		{
//			//			Debug.LogWarning("qqqqqqqqqqqqqq");
//			//			TweenRotation tweenRotati = gO.GetComponent<TweenRotation>();
//			////			gO.GetComponent<TweenRotation>().PlayForward();
//			//			Debug.LogWarning("qqqqqqqqqqqqqq: " + tweenRotati.name);
//			//			tweenRotati.PlayForward();
//			//			tweenRotati.enabled = true;
//			
//			UITweener[] tweens = GetComponents<UITweener>();
//			foreach (UITweener tw in tweens) { if (tw.tweenGroup == 3) Debug.LogWarning("uuuuuu"); tw.Play(true); }
//			//go.transform.RotateAround(Vector3.zero, new Vector3(0, go.transform.localRotation.y ,0), 180 * 0.12f * Time.deltaTime);
//		}

		//gO.GetComponent<TweenRotation>().PlayForward();
//		tweenPrimeraCartaFrontal = gO.transform.FindChild("Back").GetComponent<TweenRotation>();
//		if(tweenPrimeraCartaFrontal.tweenGroup == 1)
//			tweenPrimeraCartaFrontal.Play();


		//Animar();

//		EventDelegate.Add(gO.GetComponent<TweenRotation>().onFinished, delegate()
//		                  {
//			if(gO.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
//			{
//
//				MetodoAnim();
//			}
//		});
//		gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayForward();
//
//		EventDelegate.Add(gO.transform.FindChild("Front").GetComponent<TweenRotation>().onFinished, delegate() {
//			if(gO.transform.FindChild("Front").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
//			{
////				if(comprobarCarta)
//					MetodoAnim();
////				else{
////					gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayReverse();
////					
////					EventDelegate.Add(gO.transform.FindChild("Back").GetComponent<TweenRotation>().onFinished, delegate()
////					                  {
////						if(gO.transform.FindChild("Back").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
////						{
////							gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayReverse();
////						}
////					});
////				}
//			}
//		});
	}

	void Animar()
	{
		if(frente && voltear)
		{
			lJScript.Cards[0].GetComponent<TweenRotation>().PlayForward();
			lJScript.Cards[0].GetComponent<TweenRotation>().PlayForward();
		}
	}

	IEnumerator MetodoAnim()
	{
		//StartCoroutine(ReproducirAnim(gO));

		//TODO
		yield return new WaitForSeconds(0.1f);
		//		yield return new WaitForSeconds(1.0f);


		
//		if(lJScript.Cards[0].gameObject.GetComponent<Animator>() == null)
//		{
//			lJScript.Cards[0].gameObject.AddComponent<Animator>();
//			lJScript.Cards[0].gameObject.GetComponent<Animator>().enabled = true;
//			lJScript.Cards[0].gameObject.GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
//		}
//		
//		if(lJScript.Cards[1].gameObject.GetComponent<Animator>() == null)
//		{
//			lJScript.Cards[1].gameObject.AddComponent<Animator>();
//			lJScript.Cards[1].gameObject.GetComponent<Animator>().enabled = true;
//			lJScript.Cards[1].gameObject.GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
//
//		}
		
//		lJScript.Cards[0].gameObject.GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
//		lJScript.Cards[1].gameObject.GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
		////	
		
		
		//TODO
//		yield return new WaitForSeconds(0.4f);
		yield return new WaitForSeconds(0.2F);
		
		UITweener[] tweens = GetComponents<UITweener>();
		foreach (UITweener tw in tweens)
		{
			if (tw.tweenGroup == 3)
			{
				Debug.LogWarning("uuuuuu");
				if(comprobarCarta)
				{
					tw.delay = 0.13f;
					tw.PlayForward();

					EventDelegate.Add(tw.GetComponent<TweenRotation>().onFinished, delegate()
                 	{
          				if(tw.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
                  			{
								frente = false;
                  			}
                  		});


				}
			}
		}

		//TODO 

//		yield return new WaitForSeconds(0.15f);


//		if(lJScript.Cards[0].gameObject.GetComponent<Animator>() != null)
//		{
//			Destroy(lJScript.Cards[0].GetComponent<Animator>());
//			Debug.LogError("<----------------------------------- Tiene Componente Animator ----------------------------------->");
//
//		}
//		else
//			Debug.LogError("<----------------------------------- No Tiene Componente Animator ----------------------------------->");
//
//		if(lJScript.Cards[1].gameObject.GetComponent<Animator>() != null)
//		{
//			Destroy(lJScript.Cards[1].GetComponent<Animator>());
//			Debug.LogError("<----------------------------------- Tiene Componente Animator ----------------------------------->");
//
//
//		}
//		else
//			Debug.LogError("<----------------------------------- No Tiene Componente Animator ----------------------------------->");




		//TODO
		yield return new WaitForSeconds(0.1f);
		//		

		lJScript.Cards[0] = null;
		lJScript.Cards[1] = null;

		//TODO Aquí ya reemplace

//		foreach (Transform cardCurrent in parentCards.gameObject.transform)
//		{
//			cardCurrent.GetComponent<UIButton>().isEnabled = true;				
//		}
		
		yield return new WaitForSeconds(0.1f);

		foreach (Transform cardCurrent in parentCards.gameObject.transform)
		{
			cardCurrent.GetComponent<UIButton>().isEnabled = true;				
		}



	}
	
	IEnumerator ReproducirAnim( GameObject go)
	{


		yield return new WaitForSeconds(0.2f);
//		yield return new WaitForSeconds(1.0f);

		if(lJScript.Cards[0].gameObject.GetComponent<Animator>() == null)
		lJScript.Cards[0].gameObject.AddComponent<Animator>().enabled = true;

		if(lJScript.Cards[1].gameObject.GetComponent<Animator>() == null)
		lJScript.Cards[1].gameObject.AddComponent<Animator>().enabled = true;
		
		lJScript.Cards[0].gameObject.GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
		lJScript.Cards[1].gameObject.GetComponent<Animator>().runtimeAnimatorController = controlCarta_1;
////	



		yield return new WaitForSeconds(0.7f);

		//go.GetComponent<TweenRotation>().Play();

		if(frente)
		{
			go.GetComponent<TweenRotation>().PlayReverse();
			//go.transform.RotateAround(Vector3.zero, new Vector3(0, go.transform.localRotation.y ,0), 180 * 0.12f * Time.deltaTime);
		}




		yield return new WaitForSeconds(0.15f);

		Destroy(lJScript.Cards[0].GetComponent<Animator>());
		Destroy(lJScript.Cards[1].GetComponent<Animator>());

	

		//		
		
		foreach (Transform cardCurrent in parentCards.gameObject.transform)
		{
			cardCurrent.GetComponent<BoxCollider>().enabled = true;				
		}

		yield return new WaitForSeconds(0.1f);

		lJScript.Cards[0] = null;
		lJScript.Cards[1] = null;



//		


	}

	public void RemoveCard(){
		StartCoroutine("Remove");
	}
	IEnumerator Remove(){
		yield return new WaitForSeconds(.5f);
		Destroy(gameObject);
		lJScript.Cards[0] = null;
		lJScript.Cards[1] = null;
	}

	public void MostrarCarta(GameObject gO)
	{
		gO.GetComponent<TweenRotation>().PlayReverse(); 

		EventDelegate.Add(gO.GetComponent<TweenRotation>().onFinished, delegate()
		                  {
			// y si esa animación se reprodujo con dirección forward entonces
			if(gO.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
			{
				// debo reproducir la animación de la parte trasera de la carta en dirección forward
				//gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayForward();
				frente = true;
				
			}
		});
		// front : 1
		// back : 0

//		// Aquí reproduzco la animación de la parte frontal de la carta
//		gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayForward();		
//		
//		// Cuando termine la animación de la parte frontal de la carta
//		EventDelegate.Add(gO.transform.FindChild("Front").GetComponent<TweenRotation>().onFinished, delegate()
//		                  {
//			// y si esa animación se reprodujo con dirección forward entonces
//			if(gO.transform.FindChild("Front").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
//			{
//				// debo reproducir la animación de la parte trasera de la carta en dirección forward
//				gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayForward();
//				
//				
//			}
//		});

//		// Aquí reproduzco la animación de la parte frontal de la carta
//		gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayReverse();		
//		
//		// Cuando termine la animación de la parte frontal de la carta
//		EventDelegate.Add(gO.transform.FindChild("Front").GetComponent<TweenRotation>().onFinished, delegate()
//		                  {
//			// y si esa animación se reprodujo con dirección forward entonces
//			if(gO.transform.FindChild("Front").GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
//			{
//				// debo reproducir la animación de la parte trasera de la carta en dirección forward
//				gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayReverse();
//				
//				
//			}
//		});
	}



	public void MCM()
	{
		StartCoroutine(MC(this.gameObject));
	}

	IEnumerator MC(GameObject gO)
	{
		yield return new WaitForSeconds(1.0f);
		//gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayForward();		
		gO.transform.GetComponent<TweenRotation>().PlayForward();		
		
//		// Cuando termine la animación de la parte frontal de la carta
		EventDelegate.Add(gO.transform.GetComponent<TweenRotation>().onFinished, delegate()
		                  {
			// y si esa animación se reprodujo con dirección forward entonces
			if(gO.transform.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
			{
				// debo reproducir la animación de la parte trasera de la carta en dirección forward
				//gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayForward();
				frente = false;
				
				
			}
		});
	}

	//PowerUp de destapar las cartas
	#region PowerUp de destapar las cartas
	public void DestaparCartas()
	{

		DestaparCarta(this.gameObject);
	}
	
	public void DestaparCarta(GameObject game)
	{
//		game.GetComponent<BoxCollider>().enabled = false;
			Debug.LogWarning("Comenzo a utilizar el power up de voltear cartas");
		//gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayForward();		
		game.transform.GetComponent<TweenRotation>().PlayReverse();		
		
		//		// Cuando termine la animación de la parte frontal de la carta
		EventDelegate.Add(game.transform.GetComponent<TweenRotation>().onFinished, delegate()
		                  {
			// y si esa animación se reprodujo con dirección forward entonces
			if(game.transform.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Reverse)
			{
				// debo reproducir la animación de la parte trasera de la carta en dirección forward
				//gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayForward();
				Debug.LogWarning("Ya utilizo la primer parte del power up de voltear cartas");
				StartCoroutine(Espera(game));
			}
			
			});

	}
	
	IEnumerator Espera(GameObject ff)
	{
		yield return new WaitForSeconds(2f);
		EsconderCarta(ff);
		yield return new WaitForSeconds(2.1f);

		
	}
	
	public void EsconderCarta(GameObject game)
	{
		//gO.transform.FindChild("Back").GetComponent<TweenRotation>().PlayForward();		
		game.transform.GetComponent<TweenRotation>().PlayForward();		
		
		//		// Cuando termine la animación de la parte frontal de la carta
		EventDelegate.Add(game.transform.GetComponent<TweenRotation>().onFinished, delegate()
		                  {
			// y si esa animación se reprodujo con dirección forward entonces
			if(game.transform.GetComponent<TweenRotation>().direction == AnimationOrTween.Direction.Forward)
			{
				// debo reproducir la animación de la parte trasera de la carta en dirección forward
				//gO.transform.FindChild("Front").GetComponent<TweenRotation>().PlayForward();
				Debug.LogWarning("Ya utilizo el power up de voltear cartas");
//				game.GetComponent<BoxCollider>().enabled = true;


				game.transform.GetComponent<UIButton>().isEnabled = true;
				lJScript.RealizoPowerUpFlip = false;
				//TODO

				BScript.executePowerUpFlip = false;

				
			}
			
		});
	}
	#endregion
}
